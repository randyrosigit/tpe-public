import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { MustMatch } from '../../_helpers/must-match.validator';
import { AlertService, AuthenticationService, UserService } from '@app/_services';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

	resetForm: FormGroup;
	loading = false;
    submitted = false;
    reset_key: string = "";

	constructor(
		private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertservice: AlertService,
        private userService: UserService
	) { 
		if (this.authenticationService.currentUserValue) { 
           this.router.navigate(['/profile']);
        }

        this.route.queryParams.subscribe(params => {
	      if(params['key']) {
	        this.reset_key = params['key'];
	      }
	    });
	}

	ngOnInit() {
		this.resetForm = this.formBuilder.group({
            password: ['', [Validators.required, Validators.minLength(6)]],
            cpassword: ['', [Validators.required, Validators.minLength(6)]]
        }, {
          validator: MustMatch('password', 'cpassword')
      	});
	}

	get f() { return this.resetForm.controls; }

	onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.resetForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
	    formData.append('password', this.f.password.value);
	    formData.append('reset_key', this.reset_key);

        this.userService.reset_password(formData).pipe(first()).subscribe((data: any) => {
        	if(data.status=='ok') {
        		this.alertservice.success(data.message);
        		this.router.navigate(['/login']);
        	} else {
        		this.alertservice.error(data.message);
        	}
        	this.submitted = false;
        }, error => {
        	this.alertservice.error(error);
        	this.submitted = false;
        });
    }

}

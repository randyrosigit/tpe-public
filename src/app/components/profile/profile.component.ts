import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MustMatch } from '../../_helpers/must-match.validator';
import { Router } from '@angular/router';
import { AlertService, UserService, CartService } from '@app/_services';

import { Utils } from '../../_helpers';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit  {
 
  profileForm: FormGroup;
  loading = false;
  submitted = false;
  success = false;
  user: any[] = [];
  itemcount: string="0";

  constructor(   
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private alertservice: AlertService,
    private cartservice: CartService
  ) { }

  ngOnInit() {

    this.profileForm = this.formBuilder.group({
      firstname: ['', [Validators.required, Utils.noWhitespaceValidator]],
      lastname: ['', [Validators.required, Utils.noWhitespaceValidator]],
      email: ['', [Validators.required, Utils.noWhitespaceValidator, Validators.email]],
      password: ['', [Validators.minLength(6), Utils.noWhitespaceValidator]],
      cpassword: ['', [Utils.noWhitespaceValidator]],
    }, {
      validator: MustMatch('password', 'cpassword')
    });

    this.userService.login_check().pipe(first()).subscribe(res => {
      if(res) {
        this.loadCart();
        this.getProfileData();
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });

  }

  private getProfileData() {
    this.userService.getProfile().pipe(first()).subscribe((user: any) => {
      if(user.status=='ok'){
        this.user = user.result_set;
        this.profileForm.setValue({
          firstname: user.result_set.customer_firstname,
          lastname: user.result_set.customer_lastname,
          email: user.result_set.customer_email,
          password: '',
          cpassword:''
        });
      }
    });
  }

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }
    // convenience getter for easy access to form fields
  get f() { return this.profileForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.profileForm.invalid) {
       return;
    }

    this.loading = true;
    this.userService.update(this.profileForm.value).pipe(first()).subscribe((data:any) => {
      if(data.status=='ok') {
        this.alertservice.success('Profile Updated');
        this.loading = false;
        this.success = true;
        this.getProfileData();
      } else {
        this.alertservice.error(data.message);
      }
    },error => {
      this.alertservice.error(error);
      this.loading = false;
    });
  }

  changeProfile(file: any) {
    this.userService.uploadImage(file).subscribe((res) => {
      if(res.status=='ok') {
        this.alertservice.success('Profile picture uploaded');
        this.getProfileData();
      } else {
        this.alertservice.error(res.message);
      }
    },(err) => {
      this.alertservice.error(err);
    })
  }


}



import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';
@Component({
  selector: 'app-cart-count',
  templateUrl: './cart-count.component.html',
  styleUrls: ['./cart-count.component.scss']
})
export class CartCountComponent implements OnInit {

  @Input() itemcount: string;  
  
  constructor(private location: Location) { }

  ngOnInit() {}

  isAdvancedRoute(page): boolean {
    return this.location.path().indexOf('/'+page) > -1;
  }

}

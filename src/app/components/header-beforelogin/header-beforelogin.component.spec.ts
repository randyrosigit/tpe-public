import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderBeforeloginComponent } from './header-beforelogin.component';

describe('HeaderBeforeloginComponent', () => {
  let component: HeaderBeforeloginComponent;
  let fixture: ComponentFixture<HeaderBeforeloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderBeforeloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderBeforeloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

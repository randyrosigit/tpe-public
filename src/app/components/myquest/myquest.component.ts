//import { Component, OnInit } from '@angular/core';

import { Component, OnInit, Inject } from '@angular/core';
import {    QuestService  } from '@app/_services';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { first } from 'rxjs/operators';
export interface Sortoption {
  value: string;
  viewValue: string;
}

export interface DialogData {
  animal: string;
  name: string;
}



@Component({
  selector: 'app-myquest',
  templateUrl: './myquest.component.html',
  styleUrls: ['./myquest.component.scss']
})

export class MyquestComponent implements OnInit {

  loading = false;
  submitted = false;

  records:  any[];
  
  
	sortoptions: Sortoption[] = [
    {value: 'demo-0', viewValue: 'Option 1'},
    {value: 'mysteryhunt-1', viewValue: 'Option 2'},
    {value: 'merchant-2', viewValue: 'Option 3'}
  ];

  constructor(  private questService: QuestService, private router: Router, public dialog: MatDialog) { }

  ngOnInit() {

   //this.loadAllQuest();

  }	

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {}
    });
 
  }

   /*private loadAllQuest() {
    this.questService.getQuests().pipe(first()).subscribe(res => {
		 
        this.records = res;
        
    });
}  */

/* 
addtoCart(voucherid){

  this.questService.addToCart(voucherid)
  .pipe(first())
  .subscribe(
      data => {
         this.loading = false;
         this.router.navigate(['/login']);
      },
      error => {
         
          this.loading = false;
      });


}
 */

}



@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'details.component.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}


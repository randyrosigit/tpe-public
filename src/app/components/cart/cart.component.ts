// import { Component, OnInit, Inject, NgZone } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CartService, UserService, AlertService } from '@app/_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import * as $ from 'jquery';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {

  id="";
  loading = false;
  submitted = false;
  success = false;
  records: any[];
  voucher: any = [];  
  disabled = true;
  remove_success = false;
  qtydisabled = false;
  msg='';

  /*constructor(  private _ngZone: NgZone, private cartservice: CartService, private router: Router, private userservice: UserService, private alertservice: AlertService) { 
    (<any>window).angularComponent = {runThisFunctionFromOutside: this.runThisFunctionFromOutside, zone: _ngZone};
  }*/

  constructor( private cartservice: CartService, private router: Router, private userservice: UserService, private alertservice: AlertService) { }

  /*runThisFunctionFromOutside(id){
    console.log(id);
  }*/ 

  ngOnInit() {
    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });
  }	

  getRowSum(qty, price) {
    const sum = qty * price;
    return isNaN(sum) ? '' : sum;
  }

  ConvertToInt(val){
    return parseInt(val);
  }

  /*public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.charset = 'utf-8';
    script.defer = true;
    script.type = "text/javascript";
    body.appendChild(script);
  }*/
 
  private loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.result_set) {
        this.records = res.result_set; 
        // this.openCheckout(this);
        // this.loadScript('../assets/js/custom.js');
      }
    });
  }  

  removeFromCart(voucherid, item_id){
    this.disabled = false;
    this.voucher= {voucher_id: voucherid, item_id: item_id};
    this.cartservice.removeFromCart(this.voucher).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok') {
        this.loading = false;
        this.success = true;
        this.disabled = true;
        this.loadCart();
        this.msg = data.message;
        // this.alertservice.success('Voucher Removed from Cart');
        this.remove_success = true;   

        setTimeout( ()=>{
          this.remove_success = false;  
        }, 3500)     
      } else {
        this.loading = false;
        this.disabled = true;
        this.alertservice.error(data.message);
      }      
    },error => {
      this.loading = false;
      this.disabled = true;
      this.alertservice.error(error);
    });
  }

  QtyUpdate(qty, cart_id, item_id){
    this.qtydisabled = true;
    this.voucher= {cart_id: cart_id, item_id: item_id, quantity: qty};
    this.cartservice.CartQtyChange(this.voucher).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok') {
        this.loading = false;
        this.success = true;
        this.qtydisabled = false;
        this.loadCart();
        this.msg = data.message;
        // this.alertservice.success('Voucher Removed from Cart');
        this.remove_success = true;   

        setTimeout( ()=>{
          this.remove_success = false;  
        }, 3500)     
      } else {
        this.loading = false;
        this.qtydisabled = false;
        this.alertservice.error(data.message);
      }      
    },error => {
      this.loading = false;
      this.qtydisabled = false;
      this.alertservice.error(error);
    });
  }

  /*onSwipeLeft(item_id) {
    let elem = document.querySelector('.voucher-'+item_id);
    let style = getComputedStyle(elem);
    if(style.left=='0px') {
      $('.voucher-'+item_id).animate({left: "-=100"}, 300);
    }
  } */
  
  /*onSwipeRight(item_id) {
    let elem = document.querySelector('.voucher-'+item_id);
    let style = getComputedStyle(elem);
    if(style.left=='-100px') {
      $('.voucher-'+item_id).animate({left: "+=100"}, 300);
      $('.voucher-'+item_id).css('touch-action','pan-y');
      console.log("read");
    }
  }*/
 
}

 
import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
/*import { CartService, UserService } from '@app/_services';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';*/

@Component({
  selector: 'app-bottom-fixed-menu',
  templateUrl: './bottom-fixed-menu.component.html',
  styleUrls: ['./bottom-fixed-menu.component.scss']
})
export class BottomFixedMenuComponent implements OnInit {
  
  @Input() itemcount: string;

  constructor(private location: Location) { }

  ngOnInit() {}

  isAdvancedRoute(page): boolean {
    return this.location.path().indexOf('/'+page) > -1;
  }

}

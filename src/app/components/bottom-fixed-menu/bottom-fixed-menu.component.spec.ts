import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomFixedMenuComponent } from './bottom-fixed-menu.component';

describe('BottomFixedMenuComponent', () => {
  let component: BottomFixedMenuComponent;
  let fixture: ComponentFixture<BottomFixedMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottomFixedMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomFixedMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

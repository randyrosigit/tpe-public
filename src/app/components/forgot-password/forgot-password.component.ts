import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService, UserService } from '@app/_services';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

	forgetForm: FormGroup;
	loading = false;
    submitted = false;

	constructor(
		private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertservice: AlertService,
        private userService: UserService
	) {
		if (this.authenticationService.currentUserValue) { 
           this.router.navigate(['/profile']);
        }
	}

	ngOnInit() {
		this.forgetForm = this.formBuilder.group({
            email: ['', [Validators.required,Validators.email]]
        });
	}

	get f() { return this.forgetForm.controls; }

	onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.forgetForm.invalid) {
            return;
        }

        this.loading = true;

        this.userService.forget_password({ params: { email: this.f.email.value}}).pipe(first()).subscribe((data: any) => {
        	if(data.status=='ok') {
        		this.alertservice.success(data.message);
        	} else {
        		this.alertservice.error(data.message);
        	}
            this.loading = false;
        	this.submitted = false;
        }, error => {
        	this.alertservice.error(error);
        	this.submitted = false;
            this.loading = false;
        });
    }

}

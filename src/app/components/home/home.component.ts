import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { AlertService, HomeService } from '@app/_services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	page_id: string = 'home';
  
  constructor( private router: Router, private homeService: HomeService) { }

  ngOnInit() {
  	this.pageInfo();
  }

  pageInfo() {
  	this.homeService.getPage(this.page_id).pipe(first()).subscribe((res: any) => {
  		// console.log(res);
  	});
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VouchersHeaderComponent } from './vouchers-header.component';

describe('VouchersHeaderComponent', () => {
  let component: VouchersHeaderComponent;
  let fixture: ComponentFixture<VouchersHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VouchersHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VouchersHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

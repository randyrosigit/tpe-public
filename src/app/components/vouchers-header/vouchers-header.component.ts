import { Component, OnInit } from '@angular/core';
import {    CartService  } from '@app/_services';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-vouchers-header',
  templateUrl: './vouchers-header.component.html',
  styleUrls: ['./vouchers-header.component.scss']
})
export class VouchersHeaderComponent implements OnInit {

  records: any[];
  constructor(private cartservice: CartService,) { }

  ngOnInit() {
    this.loadCart();
  }



   loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe(res => {
        this.records = res;
    });
  }  



}

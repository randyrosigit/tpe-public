import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-header-signup',
  templateUrl: './header-signup.component.html',
  styleUrls: ['./header-signup.component.scss']
})
export class HeaderSignupComponent implements OnInit {
   
  constructor(private location: Location) { }

  ngOnInit() { 
    
  }

  isAdvancedRoute(page): boolean {
    return this.location.path().indexOf('/'+page) > -1;
  }



}

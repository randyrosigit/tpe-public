import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {    QuestService, CartService, AuthenticationService  } from '@app/_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-vouchers',
  templateUrl: './vouchers.component.html',
  styleUrls: ['./vouchers.component.scss']
})

export class VouchersComponent implements OnInit {
  
  loading = false;
  submitted = false;
  records:  any[];
   
  voucher: any = [];
  constructor(   private authenticationService: AuthenticationService, private questService: QuestService, private cartservice: CartService, private router: Router) { 

    
  }

  ngOnInit() {

    if (!this.authenticationService.currentUserValue) { 
      this.router.navigate(['/login']);
    }
    
   this.loadAllVouchers();
  }	
 


private loadAllVouchers() {
  this.questService.getVouchers().pipe(first()).subscribe(res => {
  this.records = res;
      
  });
}  


addtoCart(voucherid){
 this.voucher= {voucher_id :voucherid};
  this.cartservice.addToCart(this.voucher)
  .pipe(first())
  .subscribe(
      data => {
         this.loading = false;
         
         
         
      },
      error => {
         
          this.loading = false;
      });


}
 

}

 
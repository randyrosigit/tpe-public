import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'; 

class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';

  constructor(public src: string, public file: File) {}
} 

@Component({
  selector: 'app-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss']
})
export class ProfileHeaderComponent implements OnInit {

  @Input() user:any = [];
  @Output() changeEvent = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit() { }

  changeFile(files: FileList) {
    const file: File = files[0];
    var reader = new FileReader();
    reader.readAsDataURL(file);
    this.changeEvent.emit(file)
  } 

}


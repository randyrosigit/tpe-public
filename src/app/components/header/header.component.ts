import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	@Input() itemcount: string;
   
  constructor(private location: Location) { }

  ngOnInit() { 
    
  }

  isAdvancedRoute(page): boolean {
    return this.location.path().indexOf('/'+page) > -1;
  }



}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
 
import { AlertService, AuthenticationService, UserService } from '@app/_services';

import { SocialUser } from "angularx-social-login";
import { Utils } from '../../_helpers';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    error = false;
    returnUrl: string;
    
    user: SocialUser;
    private loggedIn: boolean;

    
    constructor(	
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertservice: AlertService,
        private authService: AuthService,
        private userService: UserService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) { 
           this.router.navigate(['/profile']);
        }
    }

    ngOnInit() {

        this.authService.authState.subscribe((user) => {
            this.user = user;
            this.loggedIn = (user != null);
          });
      


        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required, Utils.noWhitespaceValidator]],
            password: ['', [Validators.required, Utils.noWhitespaceValidator]]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }


    signInWithGoogle(): void {
		const currentObj = this;
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((user_data) => {  
            if(user_data) {
                const formData = new FormData();
                formData.append('id', user_data.id);
                formData.append('first_name', user_data.firstName);
                formData.append('last_name', user_data.lastName);
                formData.append('email', user_data.email);
                formData.append('profile_pic', user_data.photoUrl);
                formData.append('type', 'google');

                currentObj.authenticationService.fblogin(formData).pipe(first()).subscribe((data: any) => {
                    currentObj.loading = false;
                    if(data.status=='error'){
                        currentObj.alertservice.error(data.message);
                        currentObj.error = true;
                        return false;
                    }
                    currentObj.alertservice.success("Logged-in Successfully");
                    currentObj.router.navigate(['/quests']);
                },
                error => {
                    currentObj.error = true;
                    currentObj.loading = false;
                });
            } else {
                currentObj.alertservice.error('error');
            }
        });
    }
     
      signInWithFB(): void {
        const currentObj = this;
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then((user_data) => {	
            if(user_data) {
                const formData = new FormData();
                formData.append('id', user_data.id);
                formData.append('first_name', user_data.firstName);
                formData.append('last_name', user_data.lastName);
                formData.append('email', user_data.email);
                formData.append('profile_pic', user_data.photoUrl);
                formData.append('type', 'facebook');

                currentObj.authenticationService.fblogin(formData).pipe(first()).subscribe((data: any) => {
                    currentObj.loading = false;
                    if(data.status=='error'){
                        currentObj.alertservice.error(data.message);
                        currentObj.error = true;
                        return false;
                    }
                    currentObj.alertservice.success("Logged-in Successfully");
                    currentObj.router.navigate(['/quests']);
                },
                error => {
                    currentObj.error = true;
                    currentObj.loading = false;
                });
            } else {
                currentObj.alertservice.error('error');
            }
        });
      } 
     
      signOut(): void {
        this.authService.signOut();
      }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    closeme():void{
        this.error = false;
      }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value).pipe(first()).subscribe(data => {
            this.loading = false;
            if(data.status=='error'){
                this.alertservice.error(data.message);
                this.error = true;
                return false;
            }
            this.alertservice.success("Logged-in Successfully");
            this.router.navigate(['/quests']);
        },
        error => {
           this.error = true;
            this.loading = false;
        });
    }
}



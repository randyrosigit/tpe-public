import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User, Resultset } from '@app/_models';

import { Router } from '@angular/router';
import { UserService, CartService, AuthenticationService, AlertService, HomeService } from '@app/_services';

@Component({
  selector: 'app-profile',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit  {
 
  user: any[]=[];
  itemcount: string="0";
  pages: any;
  
  constructor(   
    private router: Router,
    private userService: UserService,
    private cartservice: CartService,
    private alertservice: AlertService,
    private authenticationService: AuthenticationService,
    private homeService: HomeService,
  ) { 
  }

  ngOnInit() {
    this.userService.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
        this.getProfileData();
        this.loadPages();
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });
  }

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }

  private getProfileData() {
    this.userService.getProfile().pipe(first()).subscribe(user => {
      if(user.status==='ok'){
        this.user = user.result_set;
      }
    });
  }

  loadPages() {
    this.homeService.getPageList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.pages = res.result_set;
      }
    });
  }

  signOut(): void {
    let times = new Date().getTime() - window["start"];
    this.userService.logout(times).pipe(first()).subscribe(user => {
      window["start"] = new Date().getTime();
      this.authenticationService.logout();
      this.alertservice.success('Logged out');
      this.router.navigate(['/login']);
    });
  }

  changeProfile(file: any) {
    this.userService.uploadImage(file).subscribe((res: any) => {
      if(res.status=='ok') {
        this.alertservice.success('Profile picture uploaded');
        this.getProfileData();
      } else {
        this.alertservice.error(res.message);
      }
    },(err) => {
      this.alertservice.error(err);
    })
  }

}



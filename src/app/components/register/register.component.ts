import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import {  UserService, AuthenticationService, AlertService } from '@app/_services';
import { MustMatch } from '../../_helpers/must-match.validator';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
	error = false;
    submitted = false;
    success = false;
    user: SocialUser;
    private loggedIn: boolean; 


    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private authService: AuthService,
        private alertservice: AlertService
    ) { 
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/challenges']);
        }
    }

    ngOnInit() {

     this.authService.authState.subscribe((user) => {
            this.user = user;
            this.loggedIn = (user != null);
    });
     
    this.registerForm = this.formBuilder.group({
            firstname: ['', [Validators.required, Utils.noWhitespaceValidator]],
            lastname: ['', [Validators.required,Utils.noWhitespaceValidator]],
            email: ['', [Validators.required,Utils.noWhitespaceValidator, Validators.email]],
            password: ['', [Validators.required,Utils.noWhitespaceValidator, Validators.minLength(6)]],
            cpassword: ['', [Validators.required, Utils.noWhitespaceValidator]],
        }, {
          validator: MustMatch('password', 'cpassword')
      });
    }

    // signInWithGoogle(): void {
    //     this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    //   }
     
    //   signInWithFB(): void {
    //     this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    //   } 

    signInWithGoogle(): void {
        const currentObj = this;
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((user_data) => {  
            if(user_data) {
                const formData = new FormData();
                formData.append('id', user_data.id);
                formData.append('first_name', user_data.firstName);
                formData.append('last_name', user_data.lastName);
                formData.append('email', user_data.email);
                formData.append('profile_pic', user_data.photoUrl);
                formData.append('type', 'google');

                currentObj.authenticationService.fblogin(formData).pipe(first()).subscribe((data: any) => {
                    currentObj.loading = false;
                    if(data.status=='error'){
                        currentObj.alertservice.error(data.message);
                        currentObj.error = true;
                        return false;
                    }
                    currentObj.alertservice.success("Logged-in Successfully");
                    currentObj.router.navigate(['/quests']);
                },
                error => {
                    currentObj.error = true;
                    currentObj.loading = false;
                });
            } else {
                currentObj.alertservice.error('error');
            }
        });
    }
     
      signInWithFB(): void {
        const currentObj = this;
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then((user_data) => {    
            if(user_data) {
                const formData = new FormData();
                formData.append('id', user_data.id);
                formData.append('first_name', user_data.firstName);
                formData.append('last_name', user_data.lastName);
                formData.append('email', user_data.email);
                formData.append('profile_pic', user_data.photoUrl);
                formData.append('type', 'facebook');

                currentObj.authenticationService.fblogin(formData).pipe(first()).subscribe((data: any) => {
                    currentObj.loading = false;
                    if(data.status=='error'){
                        currentObj.alertservice.error(data.message);
                        currentObj.error = true;
                        return false;
                    }
                    currentObj.alertservice.success("Logged-in Successfully");
                    currentObj.router.navigate(['/quests']);
                },
                error => {
                    currentObj.error = true;
                    currentObj.loading = false;
                });
            } else {
                currentObj.alertservice.error('error');
            }
        });
      } 
     
    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }


    closeme():void{
        this.success = false;
    }

    
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        const curobj = this;

        this.loading = true;
        this.userService.register(this.registerForm.value).pipe(first()).subscribe((regdata:any) => {
            if(regdata.status=='ok') {
                this.authenticationService.login(curobj.f.email.value, curobj.f.password.value).pipe(first()).subscribe(data => {
                    this.loading = false;
                    this.success = true;
                    if(data.status=='error'){
                        this.alertservice.error(regdata.message);
                        return false;
                    }
                    this.alertservice.success("Registered and Logged-in");
                    this.router.navigate(['/quests']);
                },error => {    
                    this.loading = false;
                });
            } else {
                this.alertservice.error(regdata.message);
            }
        }, error => {
            this.alertservice.error(error);
            this.loading = false;
        });
    }
}



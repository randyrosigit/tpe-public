import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { HomeService, CartService, UserService } from '@app/_services';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

	page_id: string;
	page_info: any =[];
  itemcount: string="0";
  panelOpenState = false;

  constructor(private router: Router, private route: ActivatedRoute, private homeService: HomeService, private cartservice: CartService, private userservice: UserService) { }

  ngOnInit() {
  	this.page_id = this.route.snapshot.paramMap.get('id');
  	this.pageInfo();

    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
      }  
    });
  }

  pageInfo() {
  	this.homeService.getPage(this.page_id).pipe(first()).subscribe((res: any) => {
  		if(res.status=='ok') {
  			this.page_info = res.result_set;
  		}
  	});
  }

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }

}

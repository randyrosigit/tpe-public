import { Component, OnInit } from '@angular/core';
import { AlertService, UserService, AuthenticationService } from '@app/_services';
import { Router } from '@angular/router';
import { User } from '@app/_models';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-header-challenges',
  templateUrl: './header-challenges.component.html',
  styleUrls: ['./header-challenges.component.scss']
}) 
export class HeaderChallengesComponent implements OnInit {
  user: User[] = [];
  currentUser: User;
  currentUserSubscription: Subscription;
  constructor( 
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService, 
    ) { }

  ngOnInit() {
    if (!this.authenticationService.currentUserValue) { 
      this.router.navigate(['/login']);
    }

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.getProfileData();
  }


  private getProfileData() {
    this.userService.getProfile().pipe(first()).subscribe(user => {
      if(user.status==='ok'){
        this.user = user.result_set;
      }else{
       
      }
    });
  }

  

}


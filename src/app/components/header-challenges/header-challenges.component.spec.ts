import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderChallengesComponent } from './header-challenges.component';

describe('HeaderChallengesComponent', () => {
  let component: HeaderChallengesComponent;
  let fixture: ComponentFixture<HeaderChallengesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderChallengesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderChallengesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

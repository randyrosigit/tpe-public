import { Component, OnInit, Inject } from '@angular/core';
import { QuestService, UserService, CartService, AlertService } from '@app/_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
 

@Component({
  selector: 'app-quests',
  templateUrl: './quests.component.html',
  styleUrls: ['./quests.component.scss'],
   
})

export class QuestsComponent implements OnInit {

 loading = false;
 vrecords:  any[] = [];
 itemcount: string="0";

 constructor(  private alertservice: AlertService, private questService: QuestService, private router: Router, private userservice: UserService, private cartservice: CartService) { }

  ngOnInit() {
    this.loadAllQuest();
    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
      }   
    });
  }	 

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }

  private loadAllQuest(categoryID='') {
    this.questService.getQuests(categoryID).pipe(first()).subscribe(res => {
      this.loading = false;
      this.vrecords = res;
    });
  }  

  receiveCategory(categoryID) {
    // this.vrecords = [];
    this.loading = true;
    this.loadAllQuest(categoryID);
  }

  private completeRequest(quest_id: string) {
    this.questService.CompleteRequest(quest_id).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok'){
        if(data.message!='') {
          this.alertservice.success(data.message);
        }
        this.router.navigate(['/quest-complete/'+quest_id]);
      } else {
        this.alertservice.error(data.message);
      }
    }, error=> {
      this.alertservice.error(error);
    });
  } 

}

 
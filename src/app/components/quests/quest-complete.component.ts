import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
 
import { AlertService, QuestService, UserService, CartService } from '@app/_services';

@Component({
  selector: 'app-quest-complete',
  templateUrl: './quest-complete.component.html',
  styleUrls: ['./quests.component.scss'],
   
})

export class QuestCompleteComponent implements OnInit {
  questForm: FormGroup;
 itemcount: string="0";
 quest_id: string;
 code: string;
 submitted = false;

 constructor(  private formBuilder: FormBuilder,private questService: QuestService, private router: Router, private route: ActivatedRoute, private userservice: UserService, private cartservice: CartService, private alertService: AlertService) { }

  ngOnInit() {
    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
      	this.quest_id = this.route.snapshot.paramMap.get('id');
        this.questForm = this.formBuilder.group({
            code: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]]
        });
        this.loadCart();
      }   
    });
  }	 

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }

  get f() { return this.questForm.controls; }

  OnSubmit() {

    this.submitted =true;

    if (this.questForm.invalid) {
      return;
    }

    const formData = new FormData();
    formData.append('quest_id', this.quest_id);
    formData.append('code', this.f.code.value);

    this.questService.codeSubmit(formData).pipe(first()).subscribe((res: any) => {
      if(res.status=='ok') {
        this.alertService.success(res.message);
        this.router.navigate(['/activities'], { queryParams: { tab: 1 } });
      } else {
        this.alertService.error(res.message);
      }
      this.submitted = false;
    });
  }

}


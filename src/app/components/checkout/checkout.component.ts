import { Component, OnInit, Inject } from '@angular/core';
import { CartService, UserService, AlertService } from '@app/_services';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent {

  loading = false;
  submitted = false;
  /*popup = false;
  step1 = false;
  step3 = false;
  step2 = false;*/
  records: any[] = [];
  voucher: any = [];
  /*payment: any[] = [];
  closeResult: string; */
  css_payment = "payment-methods-sec";
  
  constructor(  private cartservice: CartService, private router: Router, private userservice: UserService, private alertservice: AlertService) { }
  
  /*openDialog(): void {
    const dialogRef = this.dialog.open(PaymentProcessingPopupComponent, {
  		panelClass: 'payment-process-popup'
    });    
  }*/

  ngOnInit() {
    this.css_payment = "payment-methods-sec active";
    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });
  }	
 
  private loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.result_set.cart_items_count>0) {
        this.records = res.result_set; 
      } else{
        this.router.navigate(['/cart']);
      }
    });
  }  

  removeFromCart(voucherid){
    this.voucher= {voucher_id:voucherid};
    this.cartservice.removeFromCart(this.voucher).pipe(first()).subscribe((data: any) => {
      this.loading = false;
      this.loadCart();
    },error => {
      this.loading = false;
    });
  }

  getRowSum(qty, price) {
    const sum = qty * price;
    return isNaN(sum) ? '' : sum;
  }

/* openCheckout(currentobj) {
	if(this.records) {
		var totalprice = this.records.total;
	  var handler = (<any>window).StripeCheckout.configure({
		key: 'pk_test_HizPjAef61wCOntILxiF7oIz00Irp5e8PV',
		locale: 'auto',
		token: function (token: any) {  
		  currentobj.popup = true;
		  currentobj.payment= {paid_amount :totalprice/100, token: token.id};
		  currentobj.cartservice.paymentRequest(currentobj.payment).pipe(first()).subscribe(res => {
			this.step1 = true;
			if(res.status=='ok') {
			  const formData = new FormData();

			  formData.append('subtotal', currentobj.records.result_set.cart_subtotal);
			  formData.append('total', currentobj.records.result_set.cart_total);
			  formData.append('payment_method', 'Stripe');
			  formData.append('order_items', JSON.stringify(currentobj.records.result_set.cart_items));

			  currentobj.cartservice.orderAdd(formData).pipe(first()).subscribe(res1 => {
  				if(res1.status=='ok') {
  				  const formData1 = new FormData();

  				  formData1.append('token', token.id);
  				  formData1.append('captureId', res.result_set.captureId);
  				  formData1.append('order_id', res1.order_id);

  				  currentobj.cartservice.captureAmount(formData1).pipe(first()).subscribe(res2 => {
  					console.log(res2);
  				  });
  				}
			  });
			}      
		  });
		}
	  });
	}
  handler.open({
    name: 'Picky Eater',
    description: 'Vouchers',  
    amount: totalprice,
  });
}*/

/*open(content) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}*/

/* private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }*/
 
}

 
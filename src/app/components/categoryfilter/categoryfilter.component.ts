import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { first } from 'rxjs/operators';
import { QuestService } from '@app/_services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categoryfilter',
  templateUrl: './categoryfilter.component.html',
  styleUrls: ['./categoryfilter.component.scss']
})
export class CategoryfilterComponent implements OnInit {

  @Output() categoryEvent = new EventEmitter<string>();

	categorys: any[] = [];
  selectedIndex: number = 0;

  constructor(private questService: QuestService) { }

  ngOnInit() {
	   this.loadAllCategory();
  }

  private loadAllCategory() {
    this.questService.getCategory().pipe(first()).subscribe((res: any) => {
      if(res.status=='ok') {
        this.categorys = res.result_set;
      }
    });
  }

  sendCategory(categoryId: string, index=0) {
    this.selectedIndex = index;
    this.categoryEvent.emit(categoryId)
  }

}

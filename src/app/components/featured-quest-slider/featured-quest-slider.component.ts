import { Component, OnInit, Inject } from '@angular/core';

import { QuestService, AlertService, AuthenticationService } from '@app/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'app-featured-quest-slider',
  templateUrl: './featured-quest-slider.component.html',
  styleUrls: ['./featured-quest-slider.component.scss']
})


export class FeaturedQuestSliderComponent implements OnInit {

  slideConfig = {
    "slidesToShow": 1, "slidesToScroll": 1,"dots":true, "centerMode": true, "infinite": false, "arrows": false, "autoplay": true, "centerPadding": '20px',
  };

  records1: any[] = [];

  constructor( private questService: QuestService, private location: Location) {}

  ngOnInit() {
    this.loadFeaturedQuest1();
    
  }
    isAdvancedRoute(page): boolean {
    return this.location.path().indexOf('/'+page) > -1;
  }


   private loadFeaturedQuest1() {
    this.questService.getFeaturedQuests().pipe(first()).subscribe((res:any) => {
        this.records1 = res;
    });
  }
}

 
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedQuestSliderComponent } from './featured-quest-slider.component';

describe('FeaturedQuestSliderComponent', () => {
  let component: FeaturedQuestSliderComponent;
  let fixture: ComponentFixture<FeaturedQuestSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedQuestSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedQuestSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { AlertService, QuestService, UserService, CartService } from '@app/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {
	
  arecords:  any[] = [];
  crecords:  any[] = [];
  itemcount: string="0";
  tabActive = 0;

  constructor(  private alertservice: AlertService, private router: Router, private route: ActivatedRoute, private questService: QuestService, private cartservice: CartService, private userservice: UserService) { }

  ngOnInit() {
    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
        this.loadMyQuest();
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });

    this.route.queryParams.subscribe(params => {
      if(params['tab']) {
        this.tabActive = params['tab'];
      }
    });
  }

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }
  
  private loadMyQuest() {
    this.questService.getMyQuest('A').pipe(first()).subscribe((res: any) => {
        this.arecords = res.result_set;
        if(!res.result_set) {
          this.tabActive = 1;
        }
    });
    this.questService.getMyQuest('C').pipe(first()).subscribe((res: any) => {
      this.crecords = res.result_set;
    });
  } 

  private completeRequest(quest_id: string) {
    this.questService.CompleteRequest(quest_id).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok'){
        if(data.message!='') {
          this.alertservice.success(data.message);
        }
        this.router.navigate(['/quest-complete/'+quest_id]);
      } else {
        this.alertservice.error(data.message);
      }
    }, error=> {
      this.alertservice.error(error);
    });
  } 
  
}

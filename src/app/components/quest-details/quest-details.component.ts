import { Component, OnInit, Inject } from '@angular/core';
import { QuestService, UserService, CartService, AlertService } from '@app/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
 

@Component({
  selector: 'app-quest-details',
  templateUrl: './quest-details.component.html',
  styleUrls: ['./quest-details.component.scss']
})
export class QuestDetailsComponent implements OnInit {

  loading = false;
  submitted = false;
  quest_id = '';
  records:  any[];
  success = false;
  itemcount: string="0"; 

  constructor(private questService: QuestService, private router: Router, private route: ActivatedRoute,private cartservice: CartService, private userservice: UserService, private alertservice: AlertService) { }

  ngOnInit() {

   this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
        this.quest_id = this.route.snapshot.paramMap.get('id');
        this.getDetails(this.quest_id);
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });
  }	
 
  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }

  private getDetails(quest_id) {
    this.questService.getQuestDetails(quest_id).pipe(first()).subscribe((res:any) => {
      if(res.status === 'error'){
        this.router.navigate(['/quests']);
      }
      this.records = res.result_set;
    },
    error => {
      this.router.navigate(['/quests']);
    });
  }  


  acceptQuest(quest_id){
    this.questService.acceptQuest(quest_id).pipe(first()).subscribe((data: any) => {
      this.loading = false;
      this.success = true;
      if(data.status=='ok') {
        this.alertservice.success(data.message);
        this.router.navigate(['/activities']);
      } else {
        this.alertservice.error(data.message);
      }
    }, error => {
      this.loading = false;
      this.alertservice.error(error);
    });
  }

  private completeRequest(quest_id: string) {
    this.questService.CompleteRequest(quest_id).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok'){
        if(data.message!='') {
          this.alertservice.success(data.message);
        }
        this.router.navigate(['/quest-complete/'+quest_id]);
      } else {
        this.alertservice.error(data.message);
      }
    }, error=> {
      this.alertservice.error(error);
    });
  }

  isAccepted(acceptbtn): boolean {
    if(acceptbtn===true){return true;}else{return false;}
  }

  isNotAccepted(acceptbtn): boolean {
    if(acceptbtn===true){return false;}else{return true;}
  }

}

 
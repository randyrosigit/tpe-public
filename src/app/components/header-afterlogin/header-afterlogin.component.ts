import { Component, OnInit } from '@angular/core';
import { AlertService, UserService, AuthenticationService } from '@app/_services';
import { Router } from '@angular/router';
import { User } from '@app/_models';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';

  constructor(public src: string, public file: File) {}
}

@Component({
  selector: 'app-header-afterlogin',
  templateUrl: './header-afterlogin.component.html',
  styleUrls: ['./header-afterlogin.component.scss']
})
export class HeaderAfterloginComponent implements OnInit {
  user: User[] = [];
  currentUser: User;
  currentUserSubscription: Subscription;
  selectedFile: ImageSnippet;

  constructor( 
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService, 
    
    ) { }

  ngOnInit() {
    if (!this.authenticationService.currentUserValue) { 
      this.router.navigate(['/login']);
    }

    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.getProfileData();
  }


  private onSuccess() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'ok';
  }

  private onError() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'fail';
    this.selectedFile.src = '';
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {

      this.selectedFile = new ImageSnippet(event.target.result, file);

      this.selectedFile.pending = true;
      this.userService.uploadImage(this.selectedFile.file).subscribe(
        (res) => {
          this.onSuccess();
        },
        (err) => {
          this.onError();
        })
    });

    reader.readAsDataURL(file);
  }

  private getProfileData() {
    this.userService.getProfile().pipe(first()).subscribe(user => {
      if(user.status==='ok'){
        this.user = user.result_set;
      }else{
       
      }
    });
  }

  

}


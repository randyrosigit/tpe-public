import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-activities-header',
  templateUrl: './activities-header.component.html',
  styleUrls: ['./activities-header.component.scss']
})
export class ActivitiesHeaderComponent implements OnInit {

	@Input() itemcount: string;

  constructor(private location: Location) { }

  ngOnInit() {
  }
	isAdvancedRoute(page): boolean {
		return this.location.path().indexOf('/'+page) > -1;
	  }
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { CartService, UserService, AlertService } from '@app/_services';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

/*@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './processing.component.html',
  styleUrls: ['./payment.component.scss']
})
export class ProcessModalContent {

  private _loading_text: string;

  get loading_text(): string {
    // transform value for display
    return this._loading_text; 
  }

  @Input()
  set loading_text(loading_text: string) {
    console.log('prev value: ', this._loading_text);
    console.log('got name: ', loading_text);
    this._loading_text = loading_text;
  }

  @Input() step1;
  @Input() step2;
  @Input() step3;

  constructor(public activeModal: NgbActiveModal) {}
}*/

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  loading = false;
  submitted = false;
	success = true;
	closeResult: string;
  records: any = [];
  popup = false;
  Failurepopup = false;
  Successpopup = false;
  step1 = false;
  step3 = false;
  step2 = false;
  loading_text = "Validating Card";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private cartservice: CartService,
    private userservice: UserService,
	private alertservice: AlertService
  ){} 

  ngOnInit() {
    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });
  }

  private loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.result_set.cart_items_count>0) {
        this.records = res.result_set; 
        this.openCheckout(this);
      } else if(this.Successpopup) {
        this.records = res.result_set; 
      } else{
        this.router.navigate(['/cart']);
      }
    });
  }  

  getRowSum(qty, price) {
    const sum = qty * price;
    return isNaN(sum) ? '' : sum;
  }

  openCheckout(currentobj) {
    if(this.records) {
      var totalprice = this.records.cart_total;
      var handler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_kYK48ffFUsHznq9DxJMKo5rr00MGzdkvXa',
        //key: 'pk_test_HizPjAef61wCOntILxiF7oIz00Irp5e8PV',
        locale: 'auto',
        currency: 'sgd',
        token: function (token: any) {  
          currentobj.Successpopup = false;
          currentobj.Failurepopup = false;
          currentobj.popup = true;
          currentobj.payment= {paid_amount : totalprice, token: token.id};
          currentobj.cartservice.paymentRequest(currentobj.payment).pipe(first()).subscribe(res => {
            if(res.status=='ok') {
              currentobj.step1 = true;
              currentobj.loading_text="Order Processing";
              const formData = new FormData();

              formData.append('subtotal', currentobj.records.cart_subtotal);
              formData.append('total', currentobj.records.cart_total);
              formData.append('payment_method', 'Stripe');
              formData.append('order_items', JSON.stringify(currentobj.records.cart_items));

              currentobj.cartservice.orderAdd(formData).pipe(first()).subscribe(res1 => {
                if(res1.status=='ok') {
                  currentobj.step2 = true;
                  currentobj.loading_text="Amount Capturing";
                  const formData1 = new FormData();

                  formData1.append('token', token.id);
                  formData1.append('captureId', res.result_set.captureId);
                  formData1.append('order_id', res1.order_id);

                  currentobj.cartservice.captureAmount(formData1).pipe(first()).subscribe(res2 => {
                    if(res2.status=='ok') {
                      currentobj.loadCart();
                      currentobj.step3 = true;
                      currentobj.popup = false;
                      currentobj.Failurepopup = false;
                      currentobj.Successpopup = true;
                    } else {
                      currentobj.popup = false;
                      currentobj.Successpopup = false;
                      currentobj.Failurepopup = true;
                      currentobj.alertservice.error(res2.message);
                    }
                  });
                } else {
                  currentobj.popup = false;
                  currentobj.Successpopup = false;
                  currentobj.Failurepopup = true;
                  currentobj.alertservice.error(res1.message);
                }
              });
            } else {
              currentobj.popup = false;
              currentobj.Successpopup = false;
              currentobj.Failurepopup = true;
              currentobj.alertservice.error(res.message);
            }    
          });
        }
      });
    }
    handler.open({
        name: 'Picky Eater',
        description: 'Vouchers',  
        amount: totalprice*100,
        currency: 'sgd'
      });
  }

	/*open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }*/

  /*open() {
    const modalRef = this.modalService.open(ProcessModalContent);
    modalRef.componentInstance.loading_text = this.loading_text;
    modalRef.componentInstance.step1 = this.step1;
    modalRef.componentInstance.step2 = this.step2;
    modalRef.componentInstance.step3 = this.step3;
  }*/

  /*private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }*/
}


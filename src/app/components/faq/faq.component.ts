import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { HomeService, UserService, CartService } from '@app/_services';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
    
  panelOpenState = false;
  itemcount: string="0";
  faqs: any;
  
  constructor(private homeService: HomeService, private userservice: UserService, private cartservice: CartService) { }

  ngOnInit() {
  	this.getFaqs();
  	this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        this.loadCart();
      }  
    });
  }

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;
      }
    });
  }

  getFaqs() {
  	this.homeService.getFaqs().pipe(first()).subscribe((res: any) => {
  		if(res.status=='ok') {
  			this.faqs = res.result_set;
  		}
  	});
  }

}

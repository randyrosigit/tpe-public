import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-quest-header',
  templateUrl: './quest-header.component.html',
  styleUrls: ['./quest-header.component.scss']
})
export class QuestHeaderComponent implements OnInit {

  @Input() itemcount: string;

  constructor() { }

  ngOnInit() {
  }

}

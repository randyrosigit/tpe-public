import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyvouchersComponent } from './myvouchers.component';

describe('MyvouchersComponent', () => {
  let component: MyvouchersComponent;
  let fixture: ComponentFixture<MyvouchersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyvouchersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyvouchersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

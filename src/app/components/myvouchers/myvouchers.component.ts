import { Component, OnInit } from '@angular/core';
import {  AlertService, CartService, QuestService, UserService } from '@app/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import {Observable} from 'rxjs';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-myvouchers',
  templateUrl: './myvouchers.component.html',
  styleUrls: ['./myvouchers.component.scss']
})
export class MyvouchersComponent implements OnInit {
	
  precords:  any[] = [];
  erecords:  any[] = [];
  // featured_vouchers: any = [];
  vouchers: any = [];
  itemcount: string="0";
  closeResult: any;
  total_myvouchers: number = 0;
  test= "dummy";
  voucher_info: any = [];
  voucher: any;
  tabActive: 0;
  code_details: any; 
  disabling = false;
  cart_success = false;
  quantity: number = 1;
  available_quantity: number=0;
  stock_type: string;

  constructor( private router: Router, private route: ActivatedRoute, private questService: QuestService, private cartservice: CartService, private userservice: UserService, private modalService: NgbModal, private alertservice: AlertService) { }

  ngOnInit() {
    this.userservice.login_check().pipe(first()).subscribe((res: any) => {
      if(res) {
        // this.loadFeaturedVouchers();
        this.loadCart();
        this.loadMyVouchers();
        this.loadAllVouchers();
      } else {
        this.router.navigate(['/login']);
      }   
    }, error=> {
      this.router.navigate(['/login']);
    });

    this.route.queryParams.subscribe(params => {
      if(params['tab']) {
        this.tabActive = params['tab'];
      }
    });
  }

  loadCart() {
    this.cartservice.getCartList().pipe(first()).subscribe((res: any) => {
      if(res.status) {
        this.itemcount = res.result_set.cart_items_count;;
      }
    });
  }

  private loadAllVouchers(categoryID='', sort_by='') {
    this.questService.getVouchers(categoryID, sort_by).pipe(first()).subscribe(res => {
      this.vouchers = res;
    });
  } 
  
  private loadMyVouchers() {
    this.total_myvouchers = 0;
    this.questService.getMyVouchers('Purchased').pipe(first()).subscribe((res: any) => {
      if(res.status=='ok') {
        this.precords = res.result_set;
        this.total_myvouchers = parseFloat(this.total_myvouchers+res.result_set.length);

        if(this.total_myvouchers==0) {
          this.disabling = true;
        } else {
          this.disabling = false;
        }
      }
    });    
    this.questService.getMyVouchers('Earned').pipe(first()).subscribe((res: any) => {
      if(res.status=='ok') {
        this.erecords = res.result_set;
        this.total_myvouchers = parseFloat(this.total_myvouchers+res.result_set.length);
        if(this.total_myvouchers==0) {
          this.disabling = true;
        } else {
          this.disabling = false;
        }
      }
    });
  }  

  addtoCart(voucherid){
    this.voucher= {voucher_id :voucherid, quantity: this.quantity};
    this.cartservice.addToCart(this.voucher).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok') {
        this.cart_success = true;    
        setTimeout( ()=>{
          this.cart_success = false;  
        }, 3500)    
        // this.alertservice.success(data.message);
      } else {
        this.alertservice.error(data.message);
      } 
      this.loadCart();     
    }, error => {
      this.alertservice.error(error);
    });
  }

  /*private loadFeaturedVouchers() {
    this.questService.getFeaturedVouchers().pipe(first()).subscribe(res => {
      this.featured_vouchers = res;
    });
  } */

  private getVoucherDetails(voucher_id) {
    this.questService.getVoucherDetails(voucher_id).pipe(first()).subscribe((res:any) => {
      if(res.status=='ok') {
        this.voucher_info = res.result_set;
        this.stock_type = res.result_set.voucher_stock_type;
        this.available_quantity = res.result_set.voucher_stock;
      }
    });
  } 
  
  openScrollableContent(longContent, voucher_id) {
    this.getVoucherDetails(voucher_id);
    // this.modalService.open(longContent, { windowClass: 'custom-class'});
    this.modalService.open(longContent, {windowClass: 'custom-class'}).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openScrollableContent1(code, my_voucher_id) {
    this.getredeemCode(my_voucher_id);
    this.modalService.open(code, {windowClass: 'custom-class'}).result.then((result: any) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason1(reason)}`;
    });
  }

  getredeemCode(my_voucher_id) {
    this.questService.VoucherRedeem(my_voucher_id).pipe(first()).subscribe((res: any) => {
      if(res.status=='ok') {
        if(res.result_set.voucher_status =='C') {
          this.router.navigate(['/vouchers']);
        } else {
          this.code_details = res.result_set;
        }
        
      } else {
        this.alertservice.error(res.message);
      }     
    }, error => {
      this.alertservice.error(error);
    });
  }

  private getDismissReason(reason){
    this.voucher_info = [];
    this.quantity = 1;
  }

  private getDismissReason1(reason) {
    this.code_details = [];
    this.quantity = 1;
    this.loadMyVouchers();
  }

  decreaseValue() {
    if(this.quantity>1) {
      this.quantity--;
    }    
  }

  increaseValue() {
    if(this.stock_type=='0' || this.quantity<=this.available_quantity-1) {
      this.quantity++;
    } else {
      this.alertservice.error("This voucher has only " + this.available_quantity + " quantity left");
    }
  }
  
}

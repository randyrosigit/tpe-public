import { HammerGestureConfig } from '@angular/platform-browser';

export class ExtendHammerConfig extends HammerGestureConfig {
  overrides = {
      swipe: {
          direction: 6
      }
  };
}
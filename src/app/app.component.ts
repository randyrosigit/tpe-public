import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	class="app-container";
showHeadOne: boolean = true;
  title = 'Picky Eater';
  
	constructor(private location: Location ) {
		
	}
	
	ngOnInit() {
		if(this.location.path()=='/login') {
			this.class = "app-container login-container";
		}

        // your logic to know if its my home page.
    }
  
}

import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { routes } from './account.routing';
import { RouterModule } from '@angular/router';

import {SharedModule} from '../../shared-module';

import { AccountComponent } from '../../components/account/account.component';
import { ProfileComponent } from '../../components/profile/profile.component';
import { PageComponent } from '../../components/page/page.component';
import { FaqComponent } from '../../components/faq/faq.component';

@NgModule({
  declarations: [AccountComponent, ProfileComponent, PageComponent, FaqComponent],
  imports: [
    // CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class AccountModule { }

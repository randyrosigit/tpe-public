import { Routes } from '@angular/router';
import { AuthGuard } from '../../_guards';

import { AccountComponent } from '../../components/account/account.component';
import { ProfileComponent } from '../../components/profile/profile.component';
import { PageComponent } from '../../components/page/page.component';
import { FaqComponent } from '../../components/faq/faq.component';

export const routes: Routes = [
    {
	    path: 'account',
	    component: AccountComponent, 
	    canActivate: [AuthGuard]
	  },
	  {
	    path: 'profile',
	    component: ProfileComponent, 
	    canActivate: [AuthGuard]
	  },
	  {
	    path: 'faq',
	    component: FaqComponent,
	  },
	  { path: 'page/:id', 
	    component: PageComponent
	  },
];
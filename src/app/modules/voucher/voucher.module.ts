import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { routes } from './voucher.routing';
import { RouterModule } from '@angular/router';
import {SharedModule} from '../../shared-module';

import { MyvouchersComponent } from '../../components/myvouchers/myvouchers.component';
import { ActivitiesComponent } from '../../components/activities/activities.component';
import { QuestDetailsComponent } from '../../components/quest-details/quest-details.component';
import { QuestCompleteComponent } from '../../components/quests/quest-complete.component';

@NgModule({
  declarations: [ MyvouchersComponent, ActivitiesComponent, QuestDetailsComponent, QuestCompleteComponent ],
  imports: [
    // CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class VoucherModule { }

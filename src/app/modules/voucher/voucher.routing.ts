import { Routes } from '@angular/router';
import { AuthGuard } from '../../_guards';

import { MyvouchersComponent } from '../../components/myvouchers/myvouchers.component';
import { ActivitiesComponent } from '../../components/activities/activities.component';
import { QuestDetailsComponent } from '../../components/quest-details/quest-details.component';
import { QuestCompleteComponent } from '../../components/quests/quest-complete.component';

export const routes: Routes = [
  {
    path: 'vouchers',
    component: MyvouchersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'quest-complete/:id',
    component: QuestCompleteComponent,
    canActivate: [AuthGuard]
  },
  { 
  	path: 'quest/:id', 
  	component: QuestDetailsComponent,
  	canActivate: [AuthGuard]
  },
  {
    path: 'activities',
    component: ActivitiesComponent,
    canActivate: [AuthGuard]
  },
];
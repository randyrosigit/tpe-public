import { Routes } from '@angular/router';
// import { AuthGuard } from '../../_guards';

import { LoginComponent } from '../../components/login/login.component';
import { ForgotPasswordComponent } from '../../components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '../../components/reset-password/reset-password.component';
import { RegisterComponent } from '../../components/register/register.component';

export const routes: Routes = [
    {
	    path: 'login',
	    component: LoginComponent
	  },
	  {
	    path: 'register',
	    component: RegisterComponent
	  },
	  {
	    path: 'forgot-password',
	    component: ForgotPasswordComponent,
	  },
	  {
	    path: 'reset-password',
	    component: ResetPasswordComponent,
	  },
];
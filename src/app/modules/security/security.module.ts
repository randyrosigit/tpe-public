import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { routes } from './security.routing';
import { RouterModule } from '@angular/router';

import {SharedModule} from '../../shared-module';

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";

import { LoginComponent } from '../../components/login/login.component';
import { ForgotPasswordComponent } from '../../components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '../../components/reset-password/reset-password.component';
import { RegisterComponent } from '../../components/register/register.component';

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    // provider: new GoogleLoginProvider("707585013083-pbv3i4vms3v24a7hgff3jnepfflcc8v8.apps.googleusercontent.com")
    provider: new GoogleLoginProvider('32441588087-1lfs9hfbvlc6s39ds8mfar4qde8f0o70.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("260944231512706")
  }
]);
 
export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [LoginComponent, ForgotPasswordComponent, ResetPasswordComponent, RegisterComponent],
  imports: [
    // CommonModule,
    SharedModule,
    SocialLoginModule,
    RouterModule.forChild(routes)
  ],
  providers: [
  	{
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }, 
  ]
})
export class SecurityModule { }

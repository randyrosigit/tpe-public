import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { routes } from './shop.routing';
import { RouterModule } from '@angular/router';
import {SharedModule} from '../../shared-module';
import { Module as StripeModule } from "stripe-angular";

import { CartComponent } from '../../components/cart/cart.component';
import { CheckoutComponent } from '../../components/checkout/checkout.component';
import { PaymentComponent } from '../../components/payment/payment.component';

@NgModule({
  declarations: [CartComponent, CheckoutComponent, PaymentComponent],
  imports: [
    // CommonModule,
    SharedModule,
    StripeModule.forRoot(),
    RouterModule.forChild(routes)
  ]
})
export class ShopModule { }

import { Routes } from '@angular/router';
import { AuthGuard } from '../../_guards';

import { CartComponent } from '../../components/cart/cart.component';
import { CheckoutComponent } from '../../components/checkout/checkout.component';
import { PaymentComponent } from '../../components/payment/payment.component';

export const routes: Routes = [
    {
	    path: 'checkout',
	    component: CheckoutComponent, 
	    canActivate: [AuthGuard]
	  },
	  {
	    path: 'payment',
	    component: PaymentComponent, 
	    canActivate: [AuthGuard]
	  },  
	  {
	    path: 'cart',
	    component: CartComponent, 
	    canActivate: [AuthGuard]
	  },
];
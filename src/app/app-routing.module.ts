import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards';

import { HomeComponent } from './components/home/home.component';

/*import { AccountComponent } from './components/account/account.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PageComponent } from './components/page/page.component';
import { FaqComponent } from './components/faq/faq.component';*/

/*import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { RegisterComponent } from './components/register/register.component';*/

/*import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { PaymentComponent } from './components/payment/payment.component'*/;

import { QuestsComponent } from './components/quests/quests.component';

// import { VouchersComponent } from './components/vouchers/vouchers.component';


// import { MyvouchersComponent } from './components/myvouchers/myvouchers.component';
import { Page404Component } from './components/page404/page404.component';


// import { VoucherRedeemComponent } from './components/myvouchers/voucherredeem.component';

const routes: Routes = [{
    path: '',
    component: HomeComponent,
	  
  },  
  /*{
    path: 'account',
    component: AccountComponent, 
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: ProfileComponent, 
    canActivate: [AuthGuard]
  },
  {
    path: 'faq',
    component: FaqComponent,
  },
  { path: 'page/:id', 
    component: PageComponent
  },*/
  /*{
    path: 'checkout',
    component: CheckoutComponent, 
    canActivate: [AuthGuard]
  },
  {
    path: 'thankyou',
    component: ThankyouComponent, 
    canActivate: [AuthGuard]
  },
  {
    path: 'payment',
    component: PaymentComponent, 
    canActivate: [AuthGuard]
  },  
  {
    path: 'cart',
    component: CartComponent, 
    canActivate: [AuthGuard]
  },*/
  /*{
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent,
  },*/
  {
    path: 'quests',
    component: QuestsComponent
  },
  /*{
    path: 'vouchers',
    component: VouchersComponent
  },*/
   /*{
    path: 'vouchers',
    component: MyvouchersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'voucher-redeem/:id',
    component: VoucherRedeemComponent,
    canActivate: [AuthGuard]
  },*/
  
  { path: '', loadChildren: () => import('./modules/shop/shop.module').then(m => m.ShopModule) },
  { path: '', loadChildren: () => import('./modules/account/account.module').then(m => m.AccountModule) },
  { path: '', loadChildren: () => import('./modules/security/security.module').then(m => m.SecurityModule) },
  { path: '', loadChildren: () => import('./modules/voucher/voucher.module').then(m => m.VoucherModule) },
  {
    path: '**',
    component:  Page404Component,
  }
  
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

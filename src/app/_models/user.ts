﻿ 
import { Resultset } from '@app/_models';

export class User {
  status: string;
  result_set: Resultset;
  status_code: number;
  token: string;
}
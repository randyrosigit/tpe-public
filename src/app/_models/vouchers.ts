﻿export interface  Vouchers {
  voucher_merchant_id: number;
  voucher_name: string;
  voucher_description: string;
  voucher_expiry_date: string;
  voucher_sort_order: number;
  voucher_challenge_icon: string;
}
 
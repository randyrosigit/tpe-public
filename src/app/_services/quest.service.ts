﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Vouchers } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class QuestService {
    constructor(private http: HttpClient) { }

    getQuests(category_id = '') {
        return this.http.get<any[]>(`${environment.apiUrl}home/quests?filter_category_id=`+category_id);
    }

	getVouchers(category_id = '', sort_by: string='') {
        return this.http.get<any[]>(`${environment.apiUrl}home/vouchers?filter_category_id=`+category_id+`&sort_by=`+sort_by);
    }
    getFeaturedQuests() {
        return this.http.get<any[]>(`${environment.apiUrl}home/featured`);
    }
 
    getQuestDetails(quest_id) {
        return this.http.get<any[]>(`${environment.apiUrl}quest/info/`+quest_id);
    }
   
    getVoucherDetails(voucher_id) {
        return this.http.get<any[]>(`${environment.apiUrl}quest/voucher_info/`+voucher_id);
    }

    getFeaturedVouchers() {
        return this.http.get<any[]>(`${environment.apiUrl}home/featured_vouchers`);
    }

    getCategory() {
        return this.http.get<any[]>(`${environment.apiUrl}home/category`);
    }

    acceptQuest(quest_id) {
        return this.http.get<any[]>(`${environment.apiUrl}quest/accept/`+quest_id);
    }

    CompleteRequest(quest_id) {
        return this.http.get<any[]>(`${environment.apiUrl}quest/complete_request/`+quest_id);
    }

    getMyQuest(status) {
        return this.http.get<any[]>(`${environment.apiUrl}myactivities/quest?status=`+status);
    }

    getMyVouchers(type) {
        return this.http.get<any[]>(`${environment.apiUrl}myvouchers/list?type=`+type);
    }

    codeSubmit(data: any) {
        return this.http.post<any[]>(`${environment.apiUrl}quest/code_submit`, data);
    }  

    VoucherRedeem(my_voucher_id: string) {
        return this.http.get<any[]>(`${environment.apiUrl}myvouchers/code_redeem/`+my_voucher_id);
    }  
     
}
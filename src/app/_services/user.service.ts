﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from '@environments/environment';
import { User } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getProfile() {
        return this.http.get<any>(`${environment.apiUrl}customer/profile`);
    }

    register(user: User) {
        return this.http.post(`${environment.apiUrl}customer/registration`, user);
    }

    update(user: User) {
        return this.http.post(`${environment.apiUrl}customer/update`, user);
    }

    login_check() {
        return this.http.get<any>(`${environment.apiUrl}home/login_check`);
    }

    logout(time) {
        return this.http.get<any>(`${environment.apiUrl}customer/logout`, { params: { time: time } });
    }

    public uploadImage(image: File): Observable<any> {
        const formData = new FormData();
        formData.append('image', image);
        return this.http.post(`${environment.apiUrl}customer/profilepic_change`, formData);
    }

    forget_password(params) {
        return this.http.get<any>(`${environment.apiUrl}home/forget_password`, params);
    }

    reset_password(data: any) {
        return this.http.post<any>(`${environment.apiUrl}home/reset_password`, data);
    }

}
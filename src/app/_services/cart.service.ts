﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Vouchers, Payment } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class CartService {

    //Payment: any[];
    constructor(private http: HttpClient) { }

     

    addToCart(Vouchers: Vouchers) {
        return this.http.post(`${environment.apiUrl}cart/add`, Vouchers);
    }
    
    removeFromCart(Vouchers: Vouchers) {
        return this.http.post(`${environment.apiUrl}cart/cart_remove`, Vouchers);
    }

    CartQtyChange(Vouchers: Vouchers) {
        return this.http.post(`${environment.apiUrl}cart/quantity_change`, Vouchers);
    }

    getCartList() {
        return this.http.get<any[]>(`${environment.apiUrl}cart/cart_view`);
    }
    
    paymentRequest(Payment: Payment) {
        const formData = new FormData();
    
        formData.append('token', Payment.token);
        formData.append('paid_amount', Payment.paid_amount);

        return this.http.post(`${environment.apiUrl}payment/stripeTokenPay`, formData);
    }

    orderAdd(OrderData: any[]) {
        return this.http.post(`${environment.apiUrl}order/add`, OrderData);
    }

    captureAmount(CaptureData: any[]) {
        return this.http.post(`${environment.apiUrl}payment/captureAmount`, CaptureData);
    }

	
   /*  update(user: User) {
        return this.http.post(`${environment.apiUrl}customer/update`, user);
    } */

     
}
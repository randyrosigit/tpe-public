import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class HomeService {
    constructor(private http: HttpClient) { }

    getPage(page_id: string) {
        return this.http.get<any>(`${environment.apiUrl}home/page/`+page_id);
    }

    getPageList() {
    	return this.http.get<any>(`${environment.apiUrl}home/pages`);
    }

    getFaqs() {
    	return this.http.get<any>(`${environment.apiUrl}home/faqs`);
    }
}
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ExtendHammerConfig } from './_components/extendedhammerconfig.component';
import {MaterialModule} from './material-module';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
// used to create fake backend
import { ToastrModule } from 'ngx-toastr';
import { DragScrollModule } from 'ngx-drag-scroll';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AlertComponent } from './_components';

import { ActivitiesHeaderComponent } from './components/activities-header/activities-header.component';
import { CartCountComponent } from './components/cart-count/cart-count.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BottomFixedMenuComponent } from './components/bottom-fixed-menu/bottom-fixed-menu.component';
import { HeaderSignupComponent } from './components/header-signup/header-signup.component';
import { ProfileHeaderComponent } from './components/profile-header/profile-header.component';
import { FooterWhiteComponent } from './components/footer-white/footer-white.component';

@NgModule({
	imports: [ 
		CommonModule,
		RouterModule,
		MaterialModule,
		FormsModule,
		MatNativeDateModule,
		ToastrModule.forRoot(),
		ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
		DragScrollModule,
		AngularFontAwesomeModule,
		ZXingScannerModule,
		SlickCarouselModule,
		NgbModule,
	],
 	declarations: [ 
 		AlertComponent,
 		FooterComponent, 
 		BottomFixedMenuComponent, 
 		ActivitiesHeaderComponent, 
 		CartCountComponent, 
 		HeaderComponent, 
 		HeaderSignupComponent, 
 		ProfileHeaderComponent,
 		FooterWhiteComponent,
 	],
	exports: [ 
		CommonModule,
		RouterModule,
		MaterialModule,
		FormsModule,
		MatNativeDateModule,
		ToastrModule,
		ReactiveFormsModule,
		DragScrollModule,
		AngularFontAwesomeModule,
		ZXingScannerModule,
		SlickCarouselModule,
		NgbModule,
		AlertComponent,
		ActivitiesHeaderComponent,
		FooterComponent,
		BottomFixedMenuComponent,
		CartCountComponent,
		HeaderComponent,
		HeaderSignupComponent,
		ProfileHeaderComponent,
		FooterWhiteComponent
	]
})
export class SharedModule { }
/*import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MaterialModule} from './material-module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ToastrModule } from 'ngx-toastr';
import { fakeBackendProvider, JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertComponent } from './_components';
*/
import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import {SharedModule} from './shared-module';
import { ExtendHammerConfig } from './_components/extendedhammerconfig.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { fakeBackendProvider, JwtInterceptor, ErrorInterceptor } from './_helpers';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
// import { HeaderComponent } from './components/header/header.component';
// import { FooterComponent } from './components/footer/footer.component';

/*import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
*/

import { QuestsComponent } from './components/quests/quests.component';
/*import { AccountComponent } from './components/account/account.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PageComponent } from './components/page/page.component';
import { FaqComponent } from './components/faq/faq.component';*/
import { HeaderAfterloginComponent } from './components/header-afterlogin/header-afterlogin.component';
import { HeaderChallengesComponent } from './components/header-challenges/header-challenges.component';


/*import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";*/
 
// import { DragScrollModule } from 'ngx-drag-scroll';

/*import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { PaymentComponent } from './components/payment/payment.component';*/

// import { HeaderSignupComponent } from './components/header-signup/header-signup.component';
import { QuestHeaderComponent } from './components/quest-header/quest-header.component';
import { MyquestComponent } from './components/myquest/myquest.component';
// import { ActivitiesHeaderComponent } from './components/activities-header/activities-header.component';
// import { ProfileHeaderComponent } from './components/profile-header/profile-header.component';
import { VouchersHeaderComponent } from './components/vouchers-header/vouchers-header.component';
// import { VouchersComponent } from './components/vouchers/vouchers.component';
// import { Module as StripeModule } from "stripe-angular";
import { HeaderBeforeloginComponent } from './components/header-beforelogin/header-beforelogin.component';
// import { BottomFixedMenuComponent } from './components/bottom-fixed-menu/bottom-fixed-menu.component';

import { FeaturedQuestSliderComponent } from './components/featured-quest-slider/featured-quest-slider.component';
import { NgSwipeToDeleteModule } from 'ng-swipe-to-delete';
/*import { NgSwipeToDeleteModule } from './module/ng-swipe-to-delete/ng-swipe-to-delete.module';*/
// import { CartCountComponent } from './components/cart-count/cart-count.component';
import { CategoryfilterComponent } from './components/categoryfilter/categoryfilter.component';

import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { Page404Component } from './components/page404/page404.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    // HeaderComponent,
    // FooterComponent,
    /*RegisterComponent,
    LoginComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,*/
  	QuestsComponent,
    // AlertComponent,
    HeaderAfterloginComponent,
    HeaderChallengesComponent,
    /*CartComponent,
    CheckoutComponent,
    ThankyouComponent,
    PaymentComponent,*/
    // HeaderSignupComponent,
    QuestHeaderComponent,
    MyquestComponent,
    // ActivitiesHeaderComponent,
    VouchersHeaderComponent,
    // VouchersComponent,
    HeaderBeforeloginComponent,
    FeaturedQuestSliderComponent,
    // CartCountComponent,
    CategoryfilterComponent,
    EditProfileComponent,
    Page404Component,
  ],
  imports: [
    // SocialLoginModule,
    AppRoutingModule,
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [  
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }, 
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ExtendHammerConfig
    } 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { } 
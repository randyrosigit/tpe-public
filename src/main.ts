import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

document.write('<script type="text/javascript">var url = "'+environment.apiUrl+'home/time_calculation"; var start = null;  function onLoad() { start = new Date().getTime();  } function unLoad() { var time = new Date().getTime() - start; var currentUser = JSON.parse(localStorage.getItem("currentUser")); if(currentUser) { $.ajax({ url: url, type: "GET", dataType: "json", async: false, data: {time: time}, headers: { "Auth": "Bearer:"+currentUser.token }, contentType: "application/json; charset=utf-8", success: function(data) { localStorage.removeItem("currentUser"); } }); } }</script>');
// document.write('<script type="text/javascript">$(function(){ var start = null; $(window).load(function(event) { start = event.timeStamp; console.log(start); });$(window).unload(function(event) { var time = event.timeStamp - start; $.post("/collect-user-time/ajax-backend.php", {time: time}); })});</script>');



